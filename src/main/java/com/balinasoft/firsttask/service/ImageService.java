package com.balinasoft.firsttask.service;

import com.balinasoft.firsttask.dto.ImageDtoIn;
import com.balinasoft.firsttask.dto.ImageDtoOut;
import com.balinasoft.firsttask.dto.ImagePhotoTypesDtoIn;

import java.util.List;

public interface ImageService {
    ImageDtoOut uploadImage(ImageDtoIn imageDtoIn);

    ImageDtoOut updateImagePhotoTypes(ImagePhotoTypesDtoIn imagePhotoTypesDtoIn);

    void deleteImage(int id);

    List<ImageDtoOut> getImages(int page);

    List<ImageDtoOut> getImages(int page, int[] photoTypeIds);
}
