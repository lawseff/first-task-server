package com.balinasoft.firsttask.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImagePhotoTypesDtoIn {

    @ApiModelProperty(required = true)
    int id;

    @ApiModelProperty(required = true, notes = "At least one photo type id")
    Set<Integer> photoTypeIds;

    public void setPhotoTypeIds(Set<Integer> photoTypeIds) {
        if (photoTypeIds == null || photoTypeIds.size() == 0 || photoTypeIds.contains(null)) {
            throw new IllegalArgumentException();
        }
        this.photoTypeIds = photoTypeIds;
    }

}
